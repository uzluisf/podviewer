unit class App::Podviewer;

use Pod::To::HTML;

has Str  $.pod-file is rw;
has      $.css;
has Bool $.include-toc = False;
has Bool $!zenity-checked = False;

submethod TWEAK {
    $!css = do if $!css.so {
        $!css.IO.f ?? $!css !! %?RESOURCES<github.css>;
    }
    else {
        %?RESOURCES<css/github.css>;
    }
}

#| Generate temporary HTML file from Pod and return its file path.
method generate-html-file( --> Str:D ) {
    my $temp-dir = '/tmp';
    my $base-name = 'podpreviewer-' ~ now.Int ~ '.html';

    my $html-preview-file = ($temp-dir, '/', $base-name).join;

    my $html = Pod::To::HTML.render(
            $!pod-file.IO,
            header        => "<style>{$!css.IO.slurp}</style>",
            default-title => '',
        );

    # NOTE: This should be unnecessary once Pod::To::HTML allows for
    # the exclusion of the TOC.
    unless $!include-toc {
        my regex toc { '<'table \s+ 'id="TOC"' [.*]? '>' [.*]? '</'table'>' }
        $html .= subst: /<toc>/, '';
    }

    $html-preview-file.IO.spurt: $html;
    return $html-preview-file;
}

#| Show HTML file. Return the generated HTML file and whether to save it.
method show-html( --> List ) {
    my $gen-html-file = self.generate-html-file();
    my $save-html = self!zenity(
        %(
        text-info    => '',
        html         => '',
        filename     => $gen-html-file,
        ok-label     => 'Save HTML file',
        cancel-label => 'Close preview',
        width        => '1020',
        height       => '680',
        ),
        my $dont-care-var
    );

    =begin comment
    The window displaying the HTML has an 'ok-label' and 'cancel-label' buttons.
    When the 'ok-label' button is pushed, zenity returns false so 
    negate this to indicate the user wants to save the HTML file.
    =end comment

    return $gen-html-file, !$save-html;
}

#| Save the resulting html on display and return its path.
method save-html( --> List ) {
    my $save-to;
    my $is-interrupted = self!zenity(
        %(
        title             => 'Choose location for HTML document',
        file-selection    => '',
        file-filter       => 'HTML documents (html) | *.html *.htm',
        confirm-overwrite => '',
        save              => ''
        ),
        $save-to
    );
	return $save-to, $is-interrupted; 
}

#| Select Pod document to be displayed.
method select-pod( --> List ) {
    my $is-interrupted = self!zenity(
        %(
        title          => 'Choose Pod file',
        file-filter    => 'Pod files (pod, pod6) | *.pod *.pod6',
        file-selection => '',
        ),
        $!pod-file
    );
    return $!pod-file, $is-interrupted;
}

#| Show greeting message.
method show-greeting( --> Nil ) {

    constant $greeting-message = qq:to/END/;
    <span size='x-large'>Podviewer</span>
    
    <b>This is a simple application for viewing <a href="https://docs.perl6.org/language/pod">Pod files</a>. Run the application
    with a file path as an argument. To print the usage message,
    run </b><tt>podviewer -h</tt>.

    Written by Luis F. Uceta — ⎣<a href="https://gitlab.com/uzluisf">Gitlab</a>, <a href="https://github.com/uzluisf">Github</a>, <a href="http://uzluisf.gitlab.io/">Website</a>⎤

    This project's source code is located at <a href="https://gitlab.com/uzluisf/podviewer">gitlab.com/uzluisf/podviewer</a>.

    Inspired by Tomasz Gąsior's <a href="https://github.com/TomaszGasior/markdown_previewer">Markdown previewer</a>. Default CSS is from
    <a href="https://github.com/sindresorhus/github-markdown-css">github-markdown-css</a> by Sindre Sorhus, available under the MIT license.
    END

    self!zenity(
        %(
        info      => '',
        ellipsize => '',
        text      => $greeting-message,
        icon-name => 'edit',
        title     => 'Podviewer',
        ok-label  => 'OK',
        ),
        my $dont-care-var
    );
}

# Run `zenity` with passed arguments and assign its return result
# to $output.
method !zenity( %arguments, $output is rw --> Bool ) {
    #`«
    Check if 'zenity' is installed. If installed, then which's exitcode
    should be zero and this code won't execute.
    »
    if !$!zenity-checked and run('which', 'zenity', :out, :err).exitcode {
        die 'This application requires Zenity to work.';
    }
    $!zenity-checked = True;

    %arguments<window-icon> = '/usr/share/icons/Adwaita/scalable/mimetypes/' ~
                              'text-x-generic-symbolic.svg';
 
    # create an array of '--option=value' or '--option' elements.
    my @string-args = (
        ('--', .key, .value ?? '=' ~ .value !! '').join
        for %arguments
    );

    my $proc = run('zenity', @string-args, :err, :out);
    $output  = $proc.out.slurp: :close;
    return $proc.exitcode.so;
}

