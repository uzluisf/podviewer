Run either

- `$ podviewer sample.pod6` or
- `$ podviewer -o` and then select `sample.pod6` from the dialog.

![Screenshot](./screenshot.png)
