TITLE
=====

Podviewer

`podviewer` is a simple GUI application written in Raku Perl 6 for previewing (Perl 6) Pod files. It requires [Zenity](https://help.gnome.org/users/zenity/), which in turn requires GTK3 and WebKit engine.

USAGE
=====



Viewing a Pod file with slightly-modified Github Markdown CSS:

    $ podviewer sample.pod6

Viewing a Pod file with provided CSS:

    $ podviewer -css=path/to/css sample.pod6

Run **`podviewer -h`** for the usage message.

INSTALLATION
============



If you have [`zef`](https://github.com/ugexe/zef) installed, simply run `zef install podviewer`.

Alternatively, download or clone the repository (`git clone https://gitlab.com/uzluisf/podviewer.git`). Then, `zef install ./podviewer`.

AUTHOR
======

Luis F. Uceta

